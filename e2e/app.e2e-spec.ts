import { NgVideosPage } from './app.po';

describe('ng-videos App', () => {
  let page: NgVideosPage;

  beforeEach(() => {
    page = new NgVideosPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
