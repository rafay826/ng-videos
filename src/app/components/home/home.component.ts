import { Component, OnInit, OnDestroy } from '@angular/core';
import { Http } from '@angular/http';
import { VideoService } from '@app/services/video/video.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  req: any;
  homeImages: [any];

  constructor( private _video: VideoService ) { }

  ngOnInit() {
    this.req = this._video.getData().subscribe(data => this.homeImages = data)
  }

  ngOnDestroy() {
    this.req.unsubscribe()
  }

}
