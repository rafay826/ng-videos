import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

const endpoint = 'assets/json/videos.json';

@Injectable()
export class VideoService {

    private headers = new Headers({ 'Content-Type': 'application/json' });
    private options = new RequestOptions({ headers: this.headers });

    constructor( private http: Http ) { }

    getData(){
        return this.http.get(endpoint)
                .map(res => res.json())
                .catch(this.handleError)
    }
    getSlug(slug){
        return this.http.get(endpoint)
                .map(response=>{
                        let data = response.json().filter(item=>{
                        if (item.slug == slug) {
                            return item
                        }
                    })
                        if (data.length == 1){
                            return data[0]
                        }
                        return {}
                })
                .catch(this.handleError)
    }

    search(query){
    return this.http.get(endpoint)
                .map(response=>{
                        let data = []
                        let req = response.json().filter(item=>{
                        if (item.name.indexOf(query) >= 0) {
                                data.push(item)
                        }
                    })
                    return data
                })
                .catch(this.handleError)

    }

    addVideo(){
        return this.http.post(endpoint, JSON.stringify({name: 'random'}), this.options)
        .map(v => v.json())
        .catch(this.handleError)
    }

    private handleError(error:any, caught:any): any{
        console.log(error, caught)
    }

}
