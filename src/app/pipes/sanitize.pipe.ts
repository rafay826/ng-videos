import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'sanitize'
})
export class SanitizePipe implements PipeTransform {

  constructor( private sanitizer: DomSanitizer ){}

  transform(value: any, args?: any): any {
    switch(args) {
      case 'resourceUrl':
        {
          let bypass = this.sanitizer.bypassSecurityTrustResourceUrl(value);
          if (value) {
            return bypass
          }
        }
      case 'url':
        {
          let bypass = this.sanitizer.bypassSecurityTrustUrl(value);
          if (value) {
            return bypass
          }
        }
      case 'script':
        {
          let bypass = this.sanitizer.bypassSecurityTrustScript(value);
          if (value) {
            return bypass
          }
        }
      case 'style':
        {
          let bypass = this.sanitizer.bypassSecurityTrustStyle(value);
          if (value) {
            return bypass
          }
        }
      case 'html':
        {
          let bypass = this.sanitizer.bypassSecurityTrustHtml(value);
          if (value) {
            return bypass
          }
        }
      default:
        {
          let bypass = this.sanitizer.bypassSecurityTrustResourceUrl(value);
          if (value) {
            return bypass
          }
        }
    }
  }

}
