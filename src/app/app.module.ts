import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Route } from '@angular/router';
import { HttpModule } from '@angular/http';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CarouselModule } from 'ngx-bootstrap/carousel';

import { AppComponent } from './app.component';
import { VideoService } from './services/video/video.service';
import { SanitizePipe } from './pipes/sanitize.pipe';
import { HomeComponent } from './components/home/home.component';
import { SearchModule } from './modules/search/search.module';
import { VideoModule } from './modules/video/video.module';

const appRoutes: Route[] = [
  { path: '', component: HomeComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes, {useHash: true}),
    SearchModule,
    VideoModule,

    //third-party
    BsDropdownModule.forRoot(),
    CarouselModule.forRoot()
  ],
  providers: [ VideoService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
