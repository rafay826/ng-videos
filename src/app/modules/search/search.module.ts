import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Route } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchComponent } from '@app/modules/search/components/search/search.component';
import { SearchDetailComponent } from '@app/modules/search/components/search-detail/search-detail.component';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { SanitizeModule } from '../sanitize/sanitize.module';

const searchRoutes: Route[] = [
  { path: 'search', component: SearchDetailComponent }
] 

@NgModule({
  imports: [
    CommonModule,
    TypeaheadModule.forRoot(),
    RouterModule.forRoot(searchRoutes),
    FormsModule,
    ReactiveFormsModule,
    SanitizeModule
  ],
  declarations: [
    SearchComponent,
    SearchDetailComponent
  ],
  exports: [
    SearchComponent,
    SearchDetailComponent
  ]
})
export class SearchModule { }
