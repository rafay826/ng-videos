import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { VideoService } from '@app/services/video/video.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {

  req: any;
  search: any;
  videoList: [any];

  constructor( private router: Router, private _video: VideoService ) { }

  ngOnInit() {
    this.req = this._video.getData().subscribe(data => { this.videoList = data; console.log(data) })
  }

  submitSearch(event, formData) {
    let query = formData.value['q'];
    if (query) {
      this.router.navigate(['/search', {q: query}])
    }
  }

  ngOnDestroy() {
    this.req.unsubscribe();
    this.search.unsubscribe();
  }

  public customSelected:string;

}
