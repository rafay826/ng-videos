import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { VideoService } from '@app/services/video/video.service';

@Component({
  selector: 'app-search-detail',
  templateUrl: './search-detail.component.html',
  styleUrls: ['./search-detail.component.scss']
})
export class SearchDetailComponent implements OnInit {

  private routeSub: any;
  query: any;
  req: any;
  video: any;

  constructor( private route: ActivatedRoute, private _video: VideoService ) { }

  ngOnInit() {
    this.routeSub = this.route.params.subscribe(params => {
      this.query = params['q'];
      this.req = this._video.search(this.query).subscribe(data => {this.video = data})
    })
  }

  ngOnDestroy() {
    this.req.unsubscribe();
    this.routeSub.unsubscribe();
  }

}
