import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Http, Headers, RequestOptions } from '@angular/http';
import { VideoService } from '@app/services/video/video.service';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-add-video',
  templateUrl: './add-video.component.html',
  styleUrls: ['./add-video.component.scss']
})
export class AddVideoComponent implements OnInit, OnDestroy {

  private headers = new Headers({ 'Content-Type': 'application/json' });
  private options = new RequestOptions({ headers: this.headers });

  appVideoGroup: FormGroup
  addedVideo: any
  req: any
  url: any

  constructor( private _video: VideoService, private http: Http ) {
    this.url = 'assets/json/videos.json';
   }

  ngOnInit() {
    this.appVideoGroup = new FormGroup({
      name: new FormControl(''),
      slug: new FormControl(''),
      embed: new FormControl('')
    })
  }

  submitVideo() {
    console.log(this.appVideoGroup.value)
    // this.req = this._video.addVideo().subscribe(data => console.log(data))

  }

  ngOnDestroy() {
    // this.req.unsubscribe();
  }

}
