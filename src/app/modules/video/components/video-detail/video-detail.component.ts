import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { VideoService } from '@app/services/video/video.service';

@Component({
  selector: 'app-video-detail',
  templateUrl: './video-detail.component.html',
  styleUrls: ['./video-detail.component.css']
})
export class VideoDetailComponent implements OnInit, OnDestroy {

  currentSlug: string;
  video: any;
  routeSub: any;
  req: any;

  constructor(private route: ActivatedRoute, private _video: VideoService) { }

  ngOnInit() {
      this.routeSub = this.route.params.subscribe(params => {
          this.currentSlug = params['slug']
          this.req = this._video.getSlug(this.currentSlug).subscribe(data=>{
            this.video = data as [any]
          })
      })
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
    this.req.unsubscribe();
  }

}
