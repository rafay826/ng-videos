import { Component, OnInit, OnDestroy } from '@angular/core';
import { VideoService } from '@app/services/video/video.service';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent implements OnInit, OnDestroy {

  req: any;
  videoList: [any];

  constructor( private _video: VideoService ) { }

  ngOnInit() {
    this.req = this._video.getData().subscribe(data => this.videoList = data)
  }

  ngOnDestroy(){ this.req.unsubscribe() }
  
}
