import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Route } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SanitizePipe } from '@app/pipes/sanitize.pipe';
import { VideoDetailComponent } from '@app/modules/video/components/video-detail/video-detail.component';
import { VideoListComponent } from '@app/modules/video/components/video-list/video-list.component';
import { AddVideoComponent } from '@app/modules/video/components/add-video/add-video.component';
import { SanitizeModule } from '../sanitize/sanitize.module';

const videoRoutes: Route[] = [
  { path: 'add-video', component: AddVideoComponent },
  { path: 'videos', component: VideoListComponent },
  { path: 'videos/:slug', component: VideoDetailComponent }
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(videoRoutes),
    SanitizeModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    VideoDetailComponent,
    VideoListComponent,
    AddVideoComponent
  ],
  exports: [
    VideoDetailComponent,
    VideoListComponent,
    AddVideoComponent
  ]
})
export class VideoModule { }
